package Structs;

/**
 * Created by Panwar on 01/03/17.
 */
public class Garage_Car_Bean {

    private String Car_Name = "CAR_NAME";
    private String Car_Model = "CAR_MODEL";
    private String Car_Year = "CAR_YEAR";
    private String Car_Brand = "CAR_BRAND";
    private String Car_FUEL = "CAR_FUEL";
    private String Car_Segment = "CAR_SEGMENT";
    private String Car_Code = "CAR_CODE";
    private String Car_User = "CAR_USER";

    public String getCar_Name() {
        return this.Car_Name;
    }

    public void setCar_Name(String car_Name) {
        this.Car_Name = car_Name;
    }

    public String getCar_Model() {
        return this.Car_Model;
    }

    public void setCar_Model(String car_Model) {
        this.Car_Model = car_Model;
    }

    public String getCar_Year() {
        return this.Car_Year;
    }

    public void setCar_Year(String car_Year) {
        this.Car_Year = car_Year;
    }

    public String getCar_Brand() {
        return this.Car_Brand;
    }

    public void setCar_Brand(String car_Brand) {
        this.Car_Brand = car_Brand;
    }

    public String getCar_FUEL() {
        return this.Car_FUEL;
    }

    public void setCar_FUEL(String car_FUEL) {
        this.Car_FUEL = car_FUEL;
    }

    public String getCar_Segment() {
        return this.Car_Segment;
    }

    public void setCar_Segment(String car_Segment) {
        this.Car_Segment = car_Segment;
    }

    public String getCar_Code() {
        return this.Car_Code;
    }

    public void setCar_Code(String car_Code) {
        this.Car_Code = car_Code;
    }

    public String getCar_User() {
        return this.Car_User;
    }

    public void setCar_User(String car_User) {
        this.Car_User = car_User;
    }
}
